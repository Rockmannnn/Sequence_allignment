# BIOINF_project


## Basic information
Program performs a global allignment of two DNA sequences entered by a user\
Assumption is made that the DNA sequence cannot the longer than 20 characters\
Program is based on Needleman-Wunsch algorithm, link below:\
https://en.wikipedia.org/wiki/Needleman–Wunsch_algorithm

## Starting parameters:

match = 2\
mismatch = -2\
gap = -1\

## OUTPUT:

matrix with scores\
best match result of the tow sequences\
on of the best matchings available\
