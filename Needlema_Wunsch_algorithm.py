import numpy as np

def print_eveyrthing(DNA1, DNA2, matrix):
    first_modified_DNA_seq = ' ' + DNA2
    second_modified_DNA_seq = ' ' + DNA1

    print('  ', end='')
    for j in first_modified_DNA_seq:
        print(' ' + j + ' ', end='')
    print()

    for i in range(len(second_modified_DNA_seq)):
        print(second_modified_DNA_seq[i], end='')
        print(matrix[i])

def add_basic(DNA1, DNA2, matrix, gap):
    for i in range(len(DNA1)):
            matrix[0][0] = 0
            matrix[0][i+1] = matrix[0][i] +gap
    for j in range(len(DNA2)):
            matrix[0][0] = 0
            matrix[j+1][0] = matrix[j][0] +gap

    return matrix

def match_function(matrix, i, j, DNA1, DNA2, match, mismatch, gap):
    Top = matrix[i - 1][j] + gap
    Left = matrix[i][j - 1] + gap
    if DNA1[j-1] == DNA2[i-1]:
        d = match
    if DNA1[j-1] != DNA2[i-1]:
        d = mismatch
        Top_Left = matrix[i-1][j-1] + d
        matrix[i][j] = max(Top, Left, Top_Left)
    return matrix


def calculate_scores_of_given_cell(matrix, i, j, DNA1, DNA2):
    Top = matrix[i - 1][j] + gap
    Left = matrix[i][j - 1] + gap
    d = mismatch
    if DNA1[j - 1] == DNA2[i - 1]:
        d = match
    if DNA1[j - 1] != DNA2[i - 1]:
        d = mismatch
    Top_Left = matrix[i - 1][j - 1] + d
    t = max(Top, Left, Top_Left)
    return t,[Top, Left, Top_Left]

def count_best_result(matrix, DNA1, DNA2):
    Alignment1 = ''
    Alignment2 = ''
    B = ''
    i = len(DNA2)
    j = len(DNA1)

    while(i>0 or j >0):
        t,arr = calculate_scores_of_given_cell(matrix, i, j, DNA1, DNA2)

        if (i >0 and j >0 and t == arr[2]):
            #print('Top_Left')
            Alignment1 = DNA1[j-1] + Alignment1
            Alignment2 = DNA2[i-1] + Alignment2

            if DNA1[j-1] == DNA2[i-1]:
                B = '|' + B
            else:
                B = ' ' + B

            i = i - 1
            j = j - 1
        elif (i >0 and t == arr[0]):
            #print('Top')
            Alignment1 = '-' + Alignment1
            Alignment2 = DNA2[i-1] + Alignment2
            B = ' ' + B
            i = i - 1
        else:
            #print('Left')
            Alignment1 = DNA1[j-1] + Alignment1
            Alignment2 = '-' + Alignment2
            B = ' ' + B
            j = j - 1

    print(Alignment1)
    print(B)
    print(Alignment2)
    print('Score: ',matrix[len(DNA2)][len(DNA1)])

match = 2
mismatch = -2
gap = -1

DNA1 = input("Provide first DNA sequence")
DNA2 = input("Provide second DNA sequence")

if len(DNA1) >=20 or len(DNA2) >=20:
    print("One of the DNA series is too long !!!")
    DNA1 = input("Provide first DNA sequence")
    DNA2 = input("Provide second DNA sequence")

matrix = np.zeros((len(DNA2)+1,len(DNA1)+1),dtype='int16')
#print_eveyrthing(DNA1, DNA2, matrix)
matrix = add_basic(DNA1, DNA2, matrix, gap)
#print_eveyrthing(DNA1, DNA2, matrix)

for i in range(len(DNA2)):
    for j in range(len(DNA1)):
            matrix = match_function(matrix, i+1, j+1, DNA1, DNA2, match, mismatch, gap)

print_eveyrthing(DNA1, DNA2, matrix)
count_best_result(matrix, DNA1, DNA2)